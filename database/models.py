from sqlalchemy import Column, ForeignKey, Integer, VARCHAR, DateTime
from sqlalchemy.dialects.postgresql import FLOAT
from sqlalchemy.orm import relationship

from .database import Base

class StockTW(Base):
    __tablename__ = "stock_tw"

    symbol = Column(VARCHAR(64),nullable=False,primary_key=True)
    type = Column(VARCHAR(64),default="EQUITY")
    exchange = Column(VARCHAR(64),default="TWSE")
    market = Column(VARCHAR(64),default="TSE")

    datas = relationship("StockTWData",backref="general")

class StockTWData(Base):
    __tablename__ = "stock_tw_data"

    symbol = Column(VARCHAR(64),ForeignKey("stock_tw.symbol",onupdate="CASCADE",ondelete="CASCADE"),primary_key=True)
    date = Column(DateTime(timezone=True),primary_key=True)
    timeframe = Column(VARCHAR(16),nullable=False) # 1,5,10,15,30,60,D,M
    open = Column(FLOAT)
    close = Column(FLOAT)
    high = Column(FLOAT)
    low = Column(FLOAT)
    volume = Column(Integer)

