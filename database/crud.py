from sqlalchemy import cast
from sqlalchemy.dialects.postgresql import BIT

from . import database as db

def create(table,**r_data):
    '''
    insert row
    :param table: model
    :param r_data: field_name = value
    '''
    data = table(**r_data)
    session = db.DBSession()
    session.add(data)
    try:
        session.commit()
    except:
        return session, None
    session.refresh(data)
    return session, data
    
def get(table,*criterion):
    '''
    select
    :param table: model
    :param criterion: SQLAlchemy criterion
    '''
    session = db.DBSession()
    return session, session.query(table).filter(*criterion)

def update(table,*criterion, data: dict) -> int:
    '''
    update
    :param table: model
    :param criterion: SQLAlchemy criterion
    :param data: {field_name:val,...}
    '''
    session = db.DBSession()
    ret = session.query(table).filter(*criterion).update(data)
    session.commit()
    session.close()
    return ret

def delete(table,*criterion) -> int:
    '''
    delete
    :param table: model
    :param criterion: SQLAlchemy criterion
    '''
    session = db.DBSession()
    ret = session.query(table).filter(*criterion).delete()
    session.commit()
    session.close()
    return ret