from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import config as envs

SQLALCHEMY_DATABASE_URL = "%s://%s:%s@%s:%s/%s" % (envs.DB_PROTOCOL,envs.DB_USERNAME,envs.DB_PASSWD,envs.DB_HOST,envs.DB_PORT,envs.DB_NAME)
engine = create_engine(SQLALCHEMY_DATABASE_URL)
DBSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base() # inherit from this class to create ORM models

from .models import StockTW

def create_all():
    Base.metadata.create_all(engine)

def drop_all():
    Base.metadata.drop_all(engine)
