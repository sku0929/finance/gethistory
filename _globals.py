import sys
from datetime import datetime
import traceback

try:
  from dotenv import load_dotenv
  load_dotenv()
except Exception:
  pass

ENCODING = 'UTF-8'
IS_WIN32 = False
IS_LINUX = False
ARGV     = {}

if sys.platform == 'win32':
  IS_WIN32 = True
elif sys.platform == 'linux':
  IS_LINUX = True

# 0:NONE 1:ERROR 2:WARNING 3:INFO 4:DEBUG
VerboseLevel = 4 # TODO: change to 3 after stable

SEVERITY_DEBUG    = 4
SEVERITY_INFO     = 3
SEVERITY_WARN     = 2
SEVERITY_ERROR    = 1

FlagUserStopped = False

def format_curtime():
  '''
  Return datetime.now() as ISO time format string without timezone info
  '''
  return datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S')

def log(*args, **kwargs):
  level = kwargs.pop('severity')
  if level == None:
    level = 3
  if level <= 1:
    log_error(*args, **kwargs)
  elif level == 2:
    log_warning(*args, **kwargs)
  elif level == 3:
    log_info(*args, **kwargs)
  elif level >= 4:
    log_debug(*args, **kwargs)

def log_error(*args, **kwargs):
  if VerboseLevel >= 1:
    print(f"[{format_curtime()}] [ERROR]:", *args, **kwargs)

def log_warning(*args, **kwargs):
  if VerboseLevel >= 2:
    print(f"[{format_curtime()}] [WARNING]:", *args, **kwargs)

def log_info(*args, **kwargs):
  if VerboseLevel >= 3:
    print(f"[{format_curtime()}] [INFO]:", *args, **kwargs)

def log_debug(*args, **kwargs):
  if VerboseLevel >= 4:
    print(f"[{format_curtime()}] [DEBUG]:", *args, **kwargs)

ERRNO_OK           = 0
ERRNO_LOCKED       = 1
ERRNO_UNAUTH       = 2
ERRNO_MAINTENANCE  = 3
ERRNO_FAILED       = 4
ERRNO_UNAVAILABLE  = 5
ERRNO_UNACCEPTABLE = 6
ERRNO_NOTFOUND     = 7
ERRNO_EXCESSED     = 8

def readable_errno(n: int):
  for iname, ivar in globals().items():
    if not iname.startswith('ERRNO_'):
      continue
    if ivar == n:
      return iname
