import _globals
import traceback
from difflib import SequenceMatcher
from collections.abc import Iterable
from datetime import datetime

# Disable ssl verification warning
import warnings
warnings.filterwarnings("ignore")


def handle_exception(err: Exception, severity: int=_globals.SEVERITY_ERROR):
    '''
    This method format and print the excpetion for better looks
    '''
    err_info = traceback.format_exc()
    msg = f"{err}\n{err_info}\n"
    _globals.log(msg, severity=severity)

def flatten(array: list):
    ret = []
    for item in array:
        if isinstance(item, list):
            ret.extend(flatten(item))
        else:
            ret.append(item)
    return ret

def chop(xs: Iterable, n: int):
    '''
    Equally split iterable object `xs` to `n` segments
    '''
    n = max(1, n)
    return [xs[i:i+n] for i in range(0, len(xs), n)]

def diff_string(a,b):
    '''
    Get similarity rating of two strings
    '''
    return SequenceMatcher(None,a,b).ratio()

def isotime2datetime(ss:str) -> datetime:
    while not ss[-1].isdigit():
        ss = ss[:-1]
    return datetime.fromisoformat(ss)

