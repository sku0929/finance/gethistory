import os
from fugle_marketdata import RestClient
import _globals
import config as envs
from database import crud
from database import models
from datetime import datetime

def main(args):
    params = {
        "symbol":args.symbol,
        "timeframe":args.timeframe,
        "from":args.date_from,
        "to":args.date_to,
    }

    client = RestClient(api_key = envs.FUGLE_MARKET_TOKEN)
    stock = client.stock
    res = stock.historical.candles(**params)

    session, datas = crud.get(models.StockTW,models.StockTW.symbol==res["symbol"])
    if datas.count() == 0:
        stock_data = {
            "symbol": res["symbol"],
            "type": res["type"],
            "exchange": res["exchange"],
            "market": res["market"],
        }
        s_tmp,d_tmp = crud.create(models.StockTW,**stock_data)
        s_tmp.close()
    for d in res["data"]:
        tmp = {
            "symbol": res["symbol"],
            "timeframe": res["timeframe"],
            "date": datetime.strptime(d["date"], '%Y-%m-%d'),
			"open": d["open"],
			"high": d["high"],
			"low": d["low"],
			"close": d["close"],
			"volume": d["volume"],
        }
        s_tmp,d_tmp = crud.create(models.StockTWData,**tmp)
        s_tmp.close()
    
    session.close()