from argv_parser import ArgvParser
import datetime

def add_args(parser: ArgvParser):
    parser.add_argument('--symbol', type=str, help='股票代號')
    parser.add_argument('--timeframe', type=str, required=False, default="D", help='Ｋ線週期，可選 1:1分K; 5: 5分K; 10: 10分K; 15: 15分K; 30:30分K; 60: 60分K; D: 日K; W: 週K; M: 月K')
    parser.add_argument('--date_from', type=str, required=False, default=f"{datetime.datetime.today().year}-01-01", help='開始日期(yyyy-MM-dd)')
    parser.add_argument('--date_to', type=str, required=False, default=datetime.datetime.today().strftime("%Y-%m-%d"), help='結束日期(yyyy-MM-dd)')
    return parser

def load(parser: ArgvParser):
    parser = add_args(parser)
    return parser.load()