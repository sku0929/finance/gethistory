import os
DB_PROTOCOL = "postgresql"
DB_USERNAME = os.getenv("DB_USERNAME")
DB_PASSWD = os.getenv("DB_PASSWD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("DB_NAME")

FUGLE_MARKET_TOKEN = os.getenv("FUGLE_MARKET_TOKEN")