import shlex
from argparse import ArgumentParser
from utils import handle_exception

class ArgvParser(ArgumentParser):
    def __init__(self):
        super().__init__()
        self.handlers = {
            'err': []
        }
        self.add_argument('FUNC', type=str, help='功能')
        
    def parse_str(self, string):
        '''
        Parse given string as argv, instead from system argv
        '''
        try:
            return self.parse_args(shlex.split(string))
        except Exception as err:
            handle_exception(err)
            return None

    def load(self):
        return self.parse_args()

    def error(self, message):
        for func in self.handlers['err']:
            func(self, message)
        super().error(message)
    
    def add_handler(self, code:str, handler):
        '''
        Call function when an event occurs
        ### Parameters
        1. `code`: event code:
                * `err`: fires when argv parse failed
        2. `handle`: function to call, first argument is the parser itself, second is the message
        '''
        if code == 'err':
            self.handlers['err'].append(handler)
