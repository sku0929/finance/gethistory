import os
import os.path
import sys
import _globals
import importlib
from argv_parser import ArgvParser
from glob import glob
from utils import handle_exception

CMD_DIR = "acts"
importlib.import_module(CMD_DIR)

def on_argv_error(parser, msg):
    help = parser.format_help()
    help = help.replace('main.py', f"python main.py {sys.argv[1]}")
    msg = f"參數錯誤: {msg}\n{help}"
    print(msg)

def main():
    command_name = sys.argv[1]
    command_dir  = None
    for dir in glob(f"{CMD_DIR}/*"):
        if os.path.basename(dir) == command_name:
            command_dir = dir
            break
    if not command_dir:
        print(f"Command `{command_name}` not found.")
        exit(1)
    command_module = importlib.import_module(f"{CMD_DIR}.{command_name}")
    main_module    = importlib.import_module(f"{CMD_DIR}.{command_name}.main")
    argv_module    = importlib.import_module(f"{CMD_DIR}.{command_name}.argv_parse")

    parser = ArgvParser()
    parser.add_handler('err', on_argv_error)
    args = argv_module.load(parser)

    _globals.ARGV = args
    
    _globals.log_info("ARGV:", args)
    return main_module.main(args)

if __name__ == '__main__':
    err_msg  = ""+(os.getenv('CI_JOB_URL') or '')
    try:
        main()
    except (KeyboardInterrupt, InterruptedError):
        pass
    except SystemExit:
        if not _globals.FlagUserStopped:
            print(err_msg)
        exit(1)
    except Exception as err:
        print(err_msg)
        handle_exception(err)
        exit(1)